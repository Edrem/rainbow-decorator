import { Range } from "monaco-editor";
import * as monaco from "monaco-editor";

interface IRainbowDecoratorOptions {
    debounce?: number;
    colours?: string[];
    classNames?: string[];
    errorColour?: string;
    errorClassName?: string;
    tabSize?: number;
    skipErrors?: boolean;
}

class RainbowDecorator {
    private static defaultErrorColour = "rgba(128,32,32,0.3)";
    private static defaultColours = [
        "rgba(64,64,16,0.3)",
        "rgba(32,64,32,0.3)",
        "rgba(64,32,64,0.3)",
        "rgba(16,48,48,0.3)",
    ];
    private static defaultDebounceDelay = 200;

    private oldDecorators: string[][] = [];
    private oldInvalidDecorators: string[] = [];
    private editor?: monaco.editor.ICodeEditor;
    private styleSheet: HTMLStyleElement;
    private debounceTimer: number | null;
    private debounce: number;
    private errorClassName: string;
    private classNames: string[];
    private tabSize: number = 4;
    private skipErrors?: boolean;

    constructor(editor: monaco.editor.ICodeEditor, options: IRainbowDecoratorOptions = {}) {
        let depth = (options.colours || []).length || (options.classNames || []).length || RainbowDecorator.defaultColours.length;
        for (let i = 0; i < depth; i++) {
            this.oldDecorators.push([]);
        }

        let output = "";
        if (!options.classNames) {
            this.classNames = [];
            for (let i = 0; i < depth; i++) {
                const colour = options.colours ? options.colours[i] : RainbowDecorator.defaultColours[i];
                output += `.rainbow.rainbow-depth${i} {\nbackground: ${colour}; }\n`;
                this.classNames.push(`rainbow rainbow-depth${i}`);
            }
        } else {
            this.classNames = options.classNames;
        }
        if (!options.errorClassName) {
            output += `.rainbow.rainbow-error {\nbackground: ${options.errorColour || RainbowDecorator.defaultErrorColour}; }\n`;
            this.errorClassName = "rainbow rainbow-error";
        } else {
            this.errorClassName = options.errorClassName;
        }

        if (output) {
            const style = document.createElement("style");
            style.innerHTML = output;
            if (document.head) {
                document.head.appendChild(style);
            }
            this.styleSheet = style;
        }

        this.destroy = this.destroy.bind(this);
        this.applyWithDebounce = this.applyWithDebounce.bind(this);

        this.skipErrors = options.skipErrors || this.skipErrors;
        this.tabSize = options.tabSize || this.tabSize;
        this.debounce = options.debounce || RainbowDecorator.defaultDebounceDelay;
        this.editor = editor;
        this.applyWithDebounce();
        editor.onDidChangeModel(this.applyWithDebounce);
        editor.onDidChangeModelContent(this.applyWithDebounce);
        editor.onDidDispose(this.destroy);
    }

    private get depth() {
        return this.classNames.length;
    }

    private destroy(): void {
        this.editor = undefined;
        if (this.styleSheet) {
            this.styleSheet.remove();
        }
    }

    private applyWithDebounce() {
        if (!this.debounceTimer) {
            this.debounceTimer = window.setTimeout(() => {
                this.apply();
                this.debounceTimer = null;
            }, this.debounce);
            return;
        }
    }

    private apply(): void {
        if (!this.editor) {
            return;
        }

        const rainbowDecorators: monaco.editor.IModelDeltaDecoration[][] = [];
        const errorDecorators: monaco.editor.IModelDeltaDecoration[] = [];
        for (let i = 0; i < this.depth; i++) {
            rainbowDecorators.push([]);
        }

        const code = this.editor.getValue();
        const model = this.editor.getModel();
        if (!model) {
            return;
        }

        const regEx = /^[\t ]+/gm;
        const re = new RegExp("\t", "g");

        let tabs = "";
        for (let i = 0; i < this.tabSize; i++) {
            tabs += " ";
        }

        let match = regEx.exec(code);
        while (match) {
            const ma = (match[0].replace(re, tabs)).length;
            if (!this.skipErrors && ma % this.tabSize !== 0) {
                const startPos = model.getPositionAt(match.index);
                const endPos = model.getPositionAt(match.index + match[0].length);
                const decoration = {
                    range: new Range(startPos.lineNumber, startPos.column, endPos.lineNumber, endPos.column), options: {
                        className: this.errorClassName,
                        stickiness: monaco.editor.TrackedRangeStickiness.NeverGrowsWhenTypingAtEdges,
                    },
                };
                errorDecorators.push(decoration);
            } else {
                const m = match[0];
                const l = m.length;
                let o = 0;
                let n = 0;
                while (n < l) {
                    const startPos = model.getPositionAt(match.index + n);
                    if (m[n] === "\t") {
                        n++;
                    } else {
                        n += this.tabSize;
                    }
                    const endPos = model.getPositionAt(match.index + n);
                    const decoratorIndex = o % rainbowDecorators.length;
                    const decoration: monaco.editor.IModelDeltaDecoration = {
                        range: new Range(startPos.lineNumber, startPos.column, endPos.lineNumber, endPos.column), options: {
                            className: this.classNames[decoratorIndex],
                            stickiness: monaco.editor.TrackedRangeStickiness.NeverGrowsWhenTypingAtEdges,
                        }
                    };
                    rainbowDecorators[decoratorIndex].push(decoration);
                    o++;
                }
            }
            match = regEx.exec(code);
        }
        for (let i = 0; i < this.depth; i++) {
            this.oldDecorators[i] = model.deltaDecorations(this.oldDecorators[i], rainbowDecorators[i]);
        }
        this.oldInvalidDecorators = model.deltaDecorations(this.oldInvalidDecorators, errorDecorators);
    }
}

export default function rainbowDecorate(editor: monaco.editor.ICodeEditor, options?: IRainbowDecoratorOptions) {
    return new RainbowDecorator(editor, options);
}
export { rainbowDecorate };
